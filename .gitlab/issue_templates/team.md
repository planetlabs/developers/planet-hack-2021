/label ~team 
<!-- do not delete above line(s) -->

## (if you have a team name, it goes here)

_What project will this team work on?_

(link to ~pitch issue here)

## Team Members

* (link to ~introductions issue(s) here)

---

(Looking for help? If so, add a `help wanted` label to this issue, then add a comment with any relevant details)
