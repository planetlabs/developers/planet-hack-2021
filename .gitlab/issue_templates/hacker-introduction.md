/label ~introductions
<!-- do not delete above line(s) -->

## About Me

_Hi, my name is:_
<!-- your name here -->

_my pronouns are:_
<!-- examples: she/her, he/him, they/them, etc -->

**What I bring to a hack team (skills, experience, interest, etc):**
<!-- tell us about yourself: what would you like to contribute to a hack team? -->

**What kind of project I'd like to work on:**
<!-- Code? Art? Storytelling? Data analysis? Planet Hack has room for all -->
