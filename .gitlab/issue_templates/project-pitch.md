/label ~pitch
<!-- do not delete above line(s) -->

<!-- This is your opportunity to propose ("pitch") a hack project idea. Think big!-->

## (your idea goes here)

_What would a hack team for this project work towards in 2 days?_

_How would this project use Planet's data & platform?_

_What obstacles or blockers do you think this project might run into?_

_And finally... pitch your idea: why should people hack on THIS project in particular?_

---

*If you're interested in hacking on this project, add a 👍 reaction to this post.*
