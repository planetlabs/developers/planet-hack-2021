[![Gitter](https://badges.gitter.im/planet-developers/planet-hack-2021.svg)](https://gitter.im/planet-developers/planet-hack-2021?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

# Hello, World!

Welcome to Planet Hack 2021. 

## NEW in 2021: Planet Hack goes Global

**Global**. Embrace change and discover the power of Global Connection during this four day event: bring your own idea to pitch and form a team - or browse pitches and select a team to join. Meet and collaborate with data scientists, GIS professionals, software engineers, students and more from around the globe.

**Virtual**. Hackathon collaboration will take place online via Gitter chatrooms & Gitlab issues. Use the issue tracker to introduce yourself, pitch a project idea, or form a team. Beginning Oct 12th, a Gitter chat room will be open for you to chat with other Planet Hack 2021 hackers.

**Async**. With participants from timezones around the globe, this year Planet Hack is going async: while live chat will take place online, team collaboration may take place at any time around the clock. Hack project demos will be submitted as a pre-recorded video, and the hackathon demo gallery will be showcased on [Planet's Developer Center](https://developers.planet.com).

## How to Participate

- Use this repo's [issue tracker](https://gitlab.com/planetlabs/community/planet-hack-2021/-/issues)
to [introduce yourself](https://gitlab.com/planetlabs/community/planet-hack-2021/-/issues/new?issuable_template=hacker-introduction),
[pitch a project idea](https://gitlab.com/planetlabs/community/planet-hack-2021/-/issues/new?issuable_template=project-pitch),
or [form a team](https://gitlab.com/planetlabs/community/planet-hack-2021/-/issues/new?issuable_template=team).

- Use this [Gitter room](https://gitter.im/planet-developers/planet-hack-2021) to chat with other Planet Hack 2021 hackers

Note that team formation can happen anytime in the days leading up to Planet Hack - but hacking should not begin until **Thursday, October 14th**!

## Accessing Planet Data

All participants in Planet Hack 2021 are eligible for free access to Planet's high-resolution, analysis-ready mosaics of the world's tropics via the [NICFI Data Program](https://www.youtube.com/watch?v=a3qhsXzp1L8).

To access data, sign up for a free Planet account at [https://www.planet.com/nicfi/](https://www.planet.com/nicfi/). Learn more and discover additional resources at [https://developers.planet.com/nicfi/](https://developers.planet.com/nicfi/).

## Code of Conduct

All attendees, sponsors, partners, volunteers and staff are required to
agree with our [Event Code of Conduct](https://developers.planet.com/codeofconduct/).

If you experience or witness an incident and wish to make a report, [use this
form](https://docs.google.com/forms/d/e/1FAIpQLSf8b8uuyvZuqHX6Njd66Daud9PwsjlTatWjCx9rlsAMkh0KFw/viewform).

# FAQs

### I don't know anything about Planet data or satellite imagery in general, where should I start?

- You may find this [Introduction to Geospatial Data](https://developers.planet.com/planetschool/geospatial-data/) useful, as well as this [Satellite Imagery Overview](https://developers.planet.com/planetschool/satellite-imagery/)

- For a quickstart guide to usings Planet's platform & APIs, [see here](https://developers.planet.com/quickstart/)

- To browse our collection of self-guided tutorials, check out [Planet School](https://developers.planet.com/planetschool/)

### When is Planet Hack?

Hacking officially kicks off *12:01am Anywhere on Earth (AOE) Thursday October 14th*, and ends at *11:59pm Anywhere on Earth (AOE) Sunday, October 17th*.

### Tell me about demos, again?

One of the best parts of any hackathon is the opportunity to see what everyone
has been working on: demos are a fun way to show-and-tell what you worked on during the previous
week. 

Think of a demo as a Planet Hack-specific lightning talk, and not necessarily a
serious high-production-value finished-project demonstration. To take even
more of the pressure off, we use pre-recorded videos to give you as many takes
as you need to nail your demo down.

Follow Planet Hack 2021, demo videos will be showcased in a gallery here on
[developers.planet.com](https://developers.planet.com).

Remember, a demo video is not expected to be polished and flawless: think of
this as recording yourself giving a casual lightning talk. Each team/project
may submit a video of up to 15 minutes length telling about their project and
what they did during Planet Hack. Slides are optional but encouraged, as is
showing coding or project demos.

#### To submit your demo:
Each team/hacker should [fill out this
form](https://docs.google.com/forms/d/e/1FAIpQLScDax56DGwM55MZ5PKRAEDhW1Neb8JFCY6UFNkpi4cYtwXrpw/viewform?usp=sf_link)
**before 11:59pm AoE on October 17th**.

### Who can attend?
Anyone interested in exploring and experimenting with Planet's platform, APIs,
and unique dataset. You don't have to be a developer or engineer to "hack":
data scientists, geospatial pros, curious hobbyists and more are all invited.

### Can I get started early?
Brainstorming, discussion, and team formation is encouraged to begin early.
Actual hacking, however, should begin on October 14th.

### Sounds great, what do I do next?

Head on over to the [issue tracker](https://gitlab.com/planetlabs/community/planet-hack-2021/-/issues) and [introduce yourself](https://gitlab.com/planetlabs/community/planet-hack-2021/-/issues/new?issuable_template=hacker-introduction)!

### I have a question that's not answered here!
Reach out to us via [developers@planet.com](mailto:developers@planet.com).

